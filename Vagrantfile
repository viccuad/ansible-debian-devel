# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.ssh.forward_x11 = true

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
    config.cache.synced_folder_opts = {type: :nfs}
  end

  if Vagrant.has_plugin?("vagrant-sshfs")
    config.vm.synced_folder ".", "/vagrant", type: "sshfs"
  else
    config.vm.synced_folder ".", "/vagrant", disabled: true
  end

  config.vm.provider :libvirt do |libvirt|
    libvirt.memory = 1024
    libvirt.random :model => 'random' # Passthrough /dev/random
    # Set up for nested kvm:
    libvirt.machine_virtual_size = 20
    libvirt.nested = true # there's no danger in enabling nested libvirt already
    libvirt.cpu_mode = "host-passthrough" # recommended for nested kvm
  end

  config.vm.box = "debian/buster64"

  hostnames = ['debiandevel']
  hostnames.each do |name|
  config.vm.define "#{name}" do |system|
    system.vm.host_name = "#{name}"
    system.vm.provision "ansible" do |ansible|
        ansible.playbook = "debian-devel.yml"
        ansible.limit = "#{name}"
        ansible.verbose = "vv"
      end
    end
  end

  config.vm.provider :libvirt do |domain|
    domain.memory = 6144
    domain.cpus = 2
    domain.machine_virtual_size = 20
  end
end
